package com.besheater.training.javafundamentals.task3;

import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.Year;
import java.time.YearMonth;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AppTest {

    @Test
    public void parseYear_nullArguments_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseYear(null));
    }

    @Test
    public void parseYear_invalidInput_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseYear(""));
        assertThrows(IllegalArgumentException.class, () -> App.parseYear(" "));
        assertThrows(IllegalArgumentException.class, () -> App.parseYear("sdx"));
        assertThrows(IllegalArgumentException.class, () -> App.parseYear("-1"));
        assertThrows(IllegalArgumentException.class, () -> App.parseYear("-9848"));
        assertThrows(IllegalArgumentException.class,
                () -> App.parseYear(String.valueOf(App.YEAR_MIN_VALUE - 1)));
        assertThrows(IllegalArgumentException.class,
                () -> App.parseYear(String.valueOf(App.YEAR_MAX_VALUE + 1)));

    }

    @Test
    public void parseYear_validInput_returnsYear() {
        assertEquals(0, App.parseYear("0"));
        assertEquals(0, App.parseYear("000"));
        assertEquals(1, App.parseYear("1"));
        assertEquals(1, App.parseYear("01"));
        assertEquals(849, App.parseYear("849"));
        assertEquals(1652, App.parseYear("1652"));
        assertEquals(2058, App.parseYear("02058"));
        assertEquals(305845, App.parseYear("305845"));
        assertEquals(App.YEAR_MIN_VALUE, App.parseYear(String.valueOf(App.YEAR_MIN_VALUE)));
        assertEquals(App.YEAR_MAX_VALUE, App.parseYear(String.valueOf(App.YEAR_MAX_VALUE)));
    }

    @Test
    public void parseMonth_nullArguments_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseMonth(null));
    }

    @Test
    public void parseMonth_invalidInput_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> App.parseMonth(""));
        assertThrows(IllegalArgumentException.class, () -> App.parseMonth(" "));
        assertThrows(IllegalArgumentException.class, () -> App.parseMonth("Akv"));
        assertThrows(IllegalArgumentException.class, () -> App.parseMonth("-1"));
        assertThrows(IllegalArgumentException.class, () -> App.parseMonth("412551"));
        assertThrows(IllegalArgumentException.class,
                () -> App.parseMonth(String.valueOf(App.MONTH_MIN_VALUE - 1)));
        assertThrows(IllegalArgumentException.class,
                () -> App.parseMonth(String.valueOf(App.MONTH_MAX_VALUE + 1)));
    }

    @Test
    public void parseMonth_validInput_returnsMonth() {
        assertEquals(1, App.parseMonth("1"));
        assertEquals(3, App.parseMonth("03"));
        assertEquals(7, App.parseMonth("007"));
        assertEquals(11, App.parseMonth("11"));
        assertEquals(12, App.parseMonth("12"));
        assertEquals(App.MONTH_MIN_VALUE, App.parseMonth(String.valueOf(App.MONTH_MIN_VALUE)));
        assertEquals(App.MONTH_MAX_VALUE, App.parseMonth(String.valueOf(App.MONTH_MAX_VALUE)));
    }

    @Test
    public void formatYearMonthWithLeapYearInfo_invalidInput_throwsException() {
        assertThrows(DateTimeException.class,
                () -> App.formatYearMonthWithLeapYearInfo(Year.MAX_VALUE + 1, 3));
        assertThrows(DateTimeException.class,
                () -> App.formatYearMonthWithLeapYearInfo(Year.MIN_VALUE - 1, 11));
        assertThrows(DateTimeException.class,
                () -> App.formatYearMonthWithLeapYearInfo(2007, -1));
        assertThrows(DateTimeException.class,
                () -> App.formatYearMonthWithLeapYearInfo(1998, 0));
        assertThrows(DateTimeException.class,
                () -> App.formatYearMonthWithLeapYearInfo(1814, 13));
        assertThrows(DateTimeException.class,
                () -> App.formatYearMonthWithLeapYearInfo(Year.MAX_VALUE + 1, 27));
    }

    @Test
    public void formatYearMonthWithLeapYearInfo_validInput_returnsFormatted() {
        assertEquals("1 January - leap year",
                App.formatYearMonthWithLeapYearInfo(0, 1));
        assertEquals("9 March - not a leap year",
                App.formatYearMonthWithLeapYearInfo(9, 3));
        assertEquals("400 June - leap year",
                App.formatYearMonthWithLeapYearInfo(400, 6));
        assertEquals("1843 September - not a leap year",
                App.formatYearMonthWithLeapYearInfo(1843, 9));
        assertEquals("2000 December - leap year",
                App.formatYearMonthWithLeapYearInfo(2000, 12));
        assertEquals("40000 August - leap year",
                App.formatYearMonthWithLeapYearInfo(40000, 8));
    }

    @Test
    public void getYearMonthFormatted_nullArguments_throwsException() {
        assertThrows(NullPointerException.class, () -> App.getYearMonthFormatted(null));
    }

    @Test
    public void getYearMonthFormatted_validInput_returnsFormatted() {
        assertEquals("1 March",
                App.getYearMonthFormatted(YearMonth.of(0, 3)));
        assertEquals("816 January",
                App.getYearMonthFormatted(YearMonth.of(816, 1)));
        assertEquals("1723 July",
                App.getYearMonthFormatted(YearMonth.of(1723, 7)));
        assertEquals("2020 October",
                App.getYearMonthFormatted(YearMonth.of(2020, 10)));
        assertEquals("40000 December",
                App.getYearMonthFormatted(YearMonth.of(40000, 12)));
    }

    @Test
    public void getLeapYearPostfix_nullArguments_throwsException() {
        assertThrows(NullPointerException.class, () -> App.getLeapYearPostfix(null));
    }

    @Test
    public void getLeapYearPostfix_validInput_returnsLeapYearPostfix() {
        assertEquals("leap year",
                App.getLeapYearPostfix(YearMonth.of(0, 3)));
        assertEquals("not a leap year",
                App.getLeapYearPostfix(YearMonth.of(7, 11)));
        assertEquals("leap year",
                App.getLeapYearPostfix(YearMonth.of(400, 1)));
        assertEquals("not a leap year",
                App.getLeapYearPostfix(YearMonth.of(1627, 6)));
        assertEquals("leap year",
                App.getLeapYearPostfix(YearMonth.of(2000, 12)));
        assertEquals("leap year",
                App.getLeapYearPostfix(YearMonth.of(40000, 7)));
    }
}