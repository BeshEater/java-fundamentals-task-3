package com.besheater.training.javafundamentals.task3;

import java.io.PrintStream;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

public class App {
    public static final int YEAR_MIN_VALUE = 0;
    public static final int YEAR_MAX_VALUE = Year.MAX_VALUE;
    public static final int MONTH_MIN_VALUE = 1;
    public static final int MONTH_MAX_VALUE = 12;

    public static void main( String[] args ) {
        startApp(new Scanner(System.in), System.out);
    }

    public static void startApp(Scanner scanner, PrintStream out) {
        int year;
        int month;
        try {
            year = getYear(scanner, out);
            month = getMonth(scanner, out);
        } catch (IllegalArgumentException ex) {
            out.println(ex.getMessage());
            return;
        }

        out.println(formatYearMonthWithLeapYearInfo(year, month));
        out.println("That's all. Bye!");
    }

    public static int getYear(Scanner scanner, PrintStream out) {
        out.printf("Please enter year from %d to %d inclusive:\n",
                    YEAR_MIN_VALUE, YEAR_MAX_VALUE);
        String yearStr = scanner.nextLine();
        try {
            return parseYear(yearStr);
        } catch (IllegalArgumentException ex) {
            String message = String.format("Your year is not valid. " +
                            "Please enter value from %d to %d inclusive next time. Bye!",
                             YEAR_MIN_VALUE, YEAR_MAX_VALUE);
            throw new IllegalArgumentException(message, ex);
        }
    }

    public static int getMonth(Scanner scanner, PrintStream out) {
        out.printf("Please enter month from %d to %d inclusive:\n",
                    MONTH_MIN_VALUE, MONTH_MAX_VALUE);
        String monthStr = scanner.nextLine();
        try {
            return parseMonth(monthStr);
        } catch (IllegalArgumentException ex) {
            String message = String.format("Your month is not valid. " +
                            "Please enter value from %d to %d inclusive next time. Bye!",
                             MONTH_MIN_VALUE, MONTH_MAX_VALUE);
            throw new IllegalArgumentException(message, ex);
        }
    }

    public static int parseYear(String yearStr) {
        int year = Integer.parseInt(yearStr);
        if (year < YEAR_MIN_VALUE || year > YEAR_MAX_VALUE) {
            String message = String.format("Year is outside of %d to %d range",
                    YEAR_MIN_VALUE, YEAR_MAX_VALUE);
            throw new IllegalArgumentException(message);
        }
        return year;
    }

    public static int parseMonth(String monthStr) {
        int month = Integer.parseInt(monthStr);
        if (month < MONTH_MIN_VALUE || month > MONTH_MAX_VALUE) {
            String message = String.format("Month is outside of %d to %d range",
                    MONTH_MIN_VALUE, MONTH_MAX_VALUE);
            throw new IllegalArgumentException(message);
        }
        return month;
    }

    public static String formatYearMonthWithLeapYearInfo(int year, int month) {
        YearMonth yearMonth = YearMonth.of(year, month);
        return getYearMonthFormatted(yearMonth) + " - " + getLeapYearPostfix(yearMonth);
    }

    public static String getYearMonthFormatted(YearMonth yearMonth) {
        return DateTimeFormatter.ofPattern("y MMMM", Locale.ENGLISH).format(yearMonth);
    }

    public static String getLeapYearPostfix(YearMonth yearMonth) {
        return yearMonth.isLeapYear() ? "leap year" : "not a leap year";
    }
}